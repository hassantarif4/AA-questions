# frozen_string_literal: true

require './lib/questions_db'
require './lib/users'
class Replies
  attr_accessor :reply_id, :question_id, :author_id, :parent_reply_id, :body_of_reply
  def initialize(options)
    @reply_id = options['reply_id']
    @question_id = options['question_id']
    @author_id = options['author_id']
    @parent_reply_id = options['parent_reply_id']
    @body_of_reply = options['body_of_reply']
  end
  def attrs
    {
      question_id: question_id,
      author_id: author_id,
      parent_reply_id: parent_reply_id,
      body_of_reply: body_of_reply
    }
  end
  # TODO Create A new Object Save
  def save
    if self.reply_id
      QuestionsDatabase.instance.execute(<<-SQL, self.question_id, self.author_id, self.parent_reply_id , self.body_of_reply, self.reply_id)
      UPDATE 
        replies As rp
      SET 
        rp.question_id = ?, rp.author_id = ? , rp.parent_reply_id = ? , rp.body_of_reply = ?
      WHERE
        rp.reply_id = ?
      SQL
    else
      QuestionsDatabase.instance.execute(<<-SQL, self.question_id, self.author_id, self.parent_reply_id , self.body_of_reply)
      INSERT INTO 
      replies ( question_id, author_id, parent_reply_id , body_of_reply)
      VALUES 
        ( ? , ? , ?, ?)
      SQL
      self.reply_id = QuestionsDatabase.instance.last_insert_row_id
    end
    self
  end
  def find_by_id(id)
    replies_fetch = QuestionsDatabase.instance.get_first_row(<<-SQL, id)
        SELECT
            *#{' '}
        FROM#{' '}
            replies#{' '}
        WHERE#{' '}
            replies.reply_id = ?;
    SQL
    if replies_fetch.nil?
      'Hmm That Suspicious :)'
    else
      Replies.new(replies_fetch)
    end
  end

  def find_by_user_id(user_id)
    find_user_id = QuestionsDatabase.instance.get_first_row(<<-SQL, user_id)
        SELECT
            *
        FROM#{' '}
            replies
        WHERE#{' '}
            author_id = ?;
    SQL
    if find_user_id.nil?
      raise "#{user_id} does not exist"
    else
      Replies.new(find_user_id)
    end
  end

  def find_by_question_id(question_id)
    question = QuestionsDatabase.instance.get_first_row(<<-SQL, question_id)
        SELECT#{' '}
            *
        FROM#{' '}
            replies
        WHERE
            question_id = ?
    SQL
    if question.nil?
      raise "question with the id #{question} is not exists in DB"
    else
      Replies.new(question)
    end
  end

  def find_by_parent_id(parent_id)
    replies_data = QuestionsDatabase.instance.execute(<<-SQL, parent_reply_id: parent_id)
        SELECT#{' '}
            *
        FROM#{' '}
            replies
        WHERE#{' '}
            parent_reply_id = :parent_id;
    SQL
    replies_data.map { |reply_data| Replies.new(reply_data) }
  end

  def author
    Users.find_by_id(author_id)
  end

  def question
    Question.find_by_id(question_id)
  end

  def parent_reply
    Replies.find_by_id(parent_reply_id)
  end

  def child_replies
    Replies.find_by_parent_id(parent_reply_id)
  end
end
