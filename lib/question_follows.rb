# frozen_string_literal: true

require './lib/questions_db'
require './lib/questions'
require './lib/users'
class Questions_follows
  attr_reader :question_follows_id, :user_id, :question_id

  def initialize(options)
    @question_follows_id = options['question_follows_id']
    @user_id = options['user_id']
    @question_id = options['question_id']
  end
  
  def find_by_id(id)
    questions_follows_fetch = QuestionsDatabase.instance.get_first_row(<<-SQL, id)
        SELECT
            *
        FROM#{' '}
            question_follows#{' '}
        WHERE#{' '}
            question_follows.question_follow_id = ?
    SQL
    if questions_follows_fetch.nil?
      'Hmm that suspicious :)'
    else
      Questions_follows.new(questions_follows_fetch)
    end
  end

  def followers_for_question_id(question_id)
    # see the followers of the question_id and return an array of User objects
    users_data = QuestionsDatabase.instance.get_first_row(<<-SQL, question_id: question_id)
        SELECT#{' '}
            *#{' '}
        FROM#{' '}
            users
        INNER JOIN question_follows
            ON question_follows.question_id = users.user_id
        WHERE#{' '}
            question_follows.question_id = :question_id;
    SQL
    if users_data.nil?
      raise "#{users_data} is not exists in db"
    else
      users_data.map { |user| Users.new(user) }
    end
  end

  def followed_questions_for_user_id(user_id)
    questions_data = QuestionsDatabase.instance.get_first_row(<<-SQL, user_id: user_id)
        SELECT#{' '}
            *
        FROM#{' '}
            questions
        INNER JOIN question_follows
            ON question_follows.question_id = questions.question_id
        WHERE#{' '}
            question_follows.user_id: user_id;
    SQL
    if questions_data.nil?
      raise "#{questions_data} is not exists in db"
    else
      questions_data.map { |question|  Questions.new(question) }
    end
  end
  
  def most_followed_questions(n)
    most_followed_id = QuestionsDatabase.instance.get_first_row(<<-SQL, limit: n)
    SELECT
        questions.*
    FROM
        questions
    INNER JOIN question_follows
        ON question_follows.question_id = questions.question_id
    GROUP BY questions.question_id
    ORDER BY COUNT(*) DESC
    LIMIT 
        :limit
    SQL
    if most_followed_id.nil?
      raise "#{n} limit in DB is Not exists"
    else 
        most_followed_id.map { |followed| Questions.new(followed)}
    end
  end
end
