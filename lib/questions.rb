require './lib/questions_db'
require './lib/users'
require './lib/replies'
require './lib/question_follows'
require './lib/question_likes'
class Questions
  attr_accessor :title, :body, :author_id, :question_id
  def initialize(options)
    @question_id = options['question_id']
    @title = options['title']
    @author_id = options['author_id']
    @body = options['body']
  end
  # TODO: Create An object save
  def save!
    if self.question_id
      QuestionsDatabase.instance.execute(<<-SQL, self.body, self.author_id, self.title, self.question_id)
      UPDATE questions SET  body= ? , title= ? , author_id= ? where question_id = ?;
      SQL
    else
      QuestionsDatabase.instance.execute(<<-SQL, self.body, self.author_id, self.title)
      INSERT INTO
        questions (body,author_id,title)
      VALUES 
        ( ?, ?, ? )
      SQL
      self.question_id = QuestionsDatabase.instance.last_insert_row_id
    end
    self
  end
  def find_by_id(question_id)
    questions_data = QuestionsDatabase.instance.get_first_row(<<-SQL, question_id)
        SELECT#{' '}
            *
        FROM#{'    '}
            questions
        WHERE
            questions.question_id = ?
    SQL
    if questions_data.nil?
      'Hmm, that suspicious :)'
    else
      Questions.new(questions_data)
    end
  end

  def find_by_author_id(author_id)
    author_id = QuestionsDatabase.instance.get_first_row(<<-SQL, author_id: author_id)
        SELECT#{' '}
            *
        FROM
            questions
        WHERE#{' '}
            author_id = :author_id;
    SQL
    if author_id.nil?
      raise "#{author_id} does not exist in DB"
    else
      Questions.new(author_id)
    end
  end

  def author
    author_data = QuestionsDatabase.instance.get_first_row(<<-SQL, author_id: author_id)
        SELECT#{' '}
            *
        FROM#{' '}
            users
        WHERE#{' '}
            users.user_id = :author_id;
    SQL
    if author_data.nil?
      raise "#{author_id} not exists in DB"
    else
      Users.new(author_data)
    end
  end

  def replies
    Replies.find_by_question_id(question_id)
  end

  def followers
    Question_follows.followers_for_question_id(self.question_id)
  end

  def most_followed(n)
    Question_follows.most_followd_questions(n)
  end

  def likers
    Question_likes.likers_for_question_id(self.question_id)
  end

  def num_likes
    Question_likes.num_likes_for_question_id(self.question_id)
  end

  def most_liked(n)
    Question_likes.most_liked_questions(n)
  end
end

