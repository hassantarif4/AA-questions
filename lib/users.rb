# frozen_string_literal: true

require './lib/questions'
require './lib/replies'
require './lib/questions_db'
class Users 
  attr_accessor :fname, :lname, :user_id
  def initialize(options)
    @user_id = options['user_id']
    @fname = options['fname']
    @lname = options['lname']
  end 
  # TODO Add An object save
  def save!
    if self.user_id
      QuestionsDatabase.instance.execute(<<-SQL, self.fname, self.lname, self.user_id)
      UPDATE users SET fname= ? ,lname= ?  WHERE user_id = ?;
      SQL
    else
      QuestionsDatabase.instance.execute(<<-SQL, self.fname, self.lname)
      INSERT INTO
        users ( fname, lname)
      VALUES
        ( ? , ?) 
      SQL
      self.user_id = QuestionsDatabase.instance.last_insert_row_id
    end
    self
  end
  def find_by_id(user_id)
    users_data = QuestionsDatabase.instance.get_first_row(<<-SQL, user_id: user_id)
        SELECT#{' '}
            users.*
        FROM#{' '}
            users
        WHERE#{' '}
            users.user_id = :user_id;
    SQL
    # add users_add
    if users_data.nil?
      'Hmm that suspicious :)'
    else
      Users.new(users_data)
    end
  end
  
  def find_by_name(fname, lname)
    user_data = QuestionsDatabase.instance.execute(<<-SQL, fname, lname)
        SELECT#{' '}
            *
        FROM#{'  '}
            users
        WHERE#{' '}
            users.fname = ? AND users.lname = ?
    SQL
    if user_data.nil?
      'Hmm that suspicious :)'
    else
      Users.new(user_data)
    end
  end

  def authored_questions
    # return the author of the question and the questions
    Questions.find_by_author_id(user_id)
  end

  def authored_replies
    reply_instance = Replies.new('author_id' => user_id)
    reply_instance.find_by_user_id(user_id)
  end

  def followed_questions
    Question_follows.followed_questions_for_user_id(user_id)
  end

  def liked_questions
    Question_likes.liked_questions_for_user_id(user_id)
  end

  def average_karma
    average_k = QuestionsDatabase.instance.get_first_value(<<-SQL, author_id: id)
    SELECT
        CAST(COUNT(question_likes.question_id) AS FLOAT) / }
            COUNT(DISTINCT(questions.question_id)) AS avg_karma
    FROM
        questions
    LEFT OUTER JOIN question_likes#{' '}
        ON question_likes.question_id = questions.question_id
    WHERE
        questions.author_id = 1  
    SQL
  end
end


