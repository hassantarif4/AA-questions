
# frozen_string_literal: true

require './lib/questions_db'

class Question_likes
  attr_reader :like_id, :question_id, :user_id

  def initialize(options)
    @like_id = options['like_id']
    @question_id = options['question_id']
    @user_id = options['user_id']
  end
  def find_by_id(id)
    question_likes_fetch = QuestionsDatabase.instance.get_first_row(<<-SQL, id)
        SELECT#{' '}
            *
        FROM#{' '}
            question_likes
        WHERE#{' '}
            question_likes.like_id = ?;
    SQL
    if question_likes_fetch.nil?
      'Hmm That Suspicious :)'
    else
      Question_likes.new(question_likes_fetch)
    end
  end
  def likers_for_question_id(question_id)
    likes_for_question_id = QuestionsDatabase.instance.get_first_row(<<-SQL, question_id: question_id)
    SELECT
      users.*
    FROM
      users
    INNER JOIN question_likes AS likes
      ON users.user_id = likes.user_id
    WHERE
      likes.question_id = :question_id;
    SQL
    if likes_for_question_id.nil? 
      raise "#{question_id} dosen't like it by anybody"
    else 
      likes_for_question_id.map { |user_data| Users.new(user_data)}
    end
  end
  
  def num_likes_for_question_id(question_id)
    num_likes = QuestionsDatabase.instance.get_first_value(<<-SQL, question_id: question_id)
    SELECT
      COUNT(*)
    FROM
      questions AS question
    INNER JOIN question_likes AS likes
      ON question.question_id = likes.user_id
    WHERE 
      likes.question_id = :question_id;
    SQL
    if num_likes.nil?
      raise "#{question_id} is not exists in DB"
    else num_likes == 1
      num_likes
    end
  end

  def liked_questions_for_user_id(user_id)
    liked_question_if_for_user = QuestionsDatabase.instance.get_first_row(<<-SQL, user_id: user_id)
    SELECT
      *
    FROM 
      questions
    INNER JOIN question_likes AS likes
      ON questions.question_id = likes.question_id
    WHERE
      questions.author_id = :user_id;
    SQL
    if liked_questions.nil?
      raise "#{user_id} is not exists in DB"
    else
      liked_question_if_for_user.map { |question_id| Questions.new(question_id)}
    end
  end

  def most_liked_questions(n)
    most_liked_question = QuestionsDatabase.instance.execute(<<-SQL, limit: n)
    SELECT
      questions.*
    FROM 
      questions
    INNER JOIN question_likes
      ON question_likes.question_id = questions.question_id
    GROUP BY questions.question_id
    ORDER BY COUNT(*) DESC
    LIMIT 
      :limit;
    SQL
    if most_liked_question.nil?
      raise "There is not a most liked questions in your range #{n}"
    else 
      most_liked_question.map { |question|  Questions.new(question)}
    end
  end
end
